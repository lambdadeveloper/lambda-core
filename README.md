# CORE\λ::lambda();

CORE\λ::lambda() is the main library of the λ::lambda framework.



# Installation

Install using composer run in your terminal the command `composer require php-developer/lambda-core` or simply clone the project from `https://gitlab.com/php-developer/lambda`.



# Getting started

```php
require_once __DIR__.'/vendor/autoload.php';

(new \CORE\λ($_SERVER['REQUEST_URI']))
    ->route('/about', new \HOLD\APP\Controllers\Landing('about'))
    ->route('/',      new \HOLD\APP\Controllers\Landing('index'));
```

# License and terms of use

The author is not liable for misuse and/or damage caused by free use and/or distribution of this tool.

## Creative Commons 4.0

(cc) 2016, λ::Lambda - John Murowaniecki. Some rights reserved.
