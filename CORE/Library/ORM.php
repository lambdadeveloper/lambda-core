<?php
/**
 * PHP λ::lambda(); // Personal PHP7 Framework
 *
 * PHP version 7
 *
 * @category Framework
 * @package  Lambda
 * @author   John Murowaniecki <jmurowaniecki@gmail.com>
 * @license  Creative Commons 4.0 - Some rights reserved.
 * @link     https://gitlab.com/php-developer/lambda
 **/

namespace CORE\Library;

use \CORE\λ as λ;

/**
 * Library class ORM
 *
 * @category CORE_Library
 * @package  Lambda
 * @author   John Murowaniecki <jmurowaniecki@gmail.com>
 * @license  Creative Commons 4.0 - Some rights reserved.
 * @link     https://gitlab.com/php-developer/lambda
 **/
class ORM
{

    public $table;

    public $where = [];

    public $structure = [];

    /**
     * [__construct description]
     * @param String $collection [description]
     * @param Array  $fields     [description]
     */
    public function __construct(String $collection = null, $fields = [])
    {
        $this->DB = new \CORE\Drivers\PDO_MySQL;

        if ($collection) {
            $this->table = strtolower($collection);
        }
        if ($fields) {
            $this->fields;
        }
        $this->checkStructure();
    }

    /**
    * [collection description]
    * @param  [type] $collection [description]
    * @param  array  $options    [description]
    * @return [type]             [description]
    */
    public function collection(String $collection = null, Array $options = [])
    {
        // $this->table     = strtolower($collection);
        // $this->structure = $options['structure']['fields'] ?? [];
        return $this;
    }

    /**
     * [checkStructure description]
     * @return [type] [description]
     */
    public function checkStructure()
    {
        if (!$this->DB->query("SHOW TABLES LIKE '{$this->table}'")->fetch()) {
            $this->DB->exec("CREATE TABLE IF NOT EXISTS `{$this->table}` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `created` datetime NOT NULL,
                `updated` datetime,
                `status` int(1) NOT NULL DEFAULT '0',
                PRIMARY KEY (`id`)
                ) ENGINE=MyISAM  DEFAULT CHARSET=utf8");
        }
        $fields = [];

        foreach ($this->DB->query("SELECT *
            FROM information_schema.COLUMNS
            WHERE TABLE_NAME  = '{$this->table}'
              AND COLUMN_NAME = '{$field}';") as $check) {
                  $fields[$field] = $check;
        }
        foreach ($this->fields as $field => $options) {
            if (!key_exists($field, $fields)) {
                $nullable = !$options['null'] ? 'NOT NULL' : '';
                $defaults = !$options['default'] ? '' : "DEFAULT '{$options['default']}'";
                $size     =  $options['size'] ?? '';

                $this->DB->exec("ALTER TABLE `{$this->table}`
                    ADD {$field} {$options['type']}{$size} {$nullable} {$defaults}");
            }
        }
    }

    /**
     * [where description]
     * @param  [type]  $fields [description]
     * @param  boolean $value  [description]
     * @return [type]          [description]
     */
    public function where($fields, $value = false)
    {
        if (!is_array($fields)) {
            $fields = [$fields => $value];
        }

        foreach ($fields as $field => $value) {
            if ($this->structure && !array_key_exists($field, $this->structure)) {
                die("invalid field $field - look for ".implode(', ', array_keys($this->structure)));
            }
            $this->where[] = "{$field} = '{$value}'";
        }
        return $this;
    }

    /**
     * [where_in description]
     * @param  [type]  $field  [description]
     * @param  boolean $values [description]
     * @return [type]          [description]
     */
    public function where_in($field, $values = false)
    {
        $this->where[] = "{$field} IN({$values})";
        return $this;
    }

    /**
     * [like description]
     * @param  [type] $field [description]
     * @param  [type] $value [description]
     * @return [type]        [description]
     */
    public function like($field, $value)
    {
        $this->where[] = "{$field} LIKE '{$value}'";

        return $this;
    }


    /**
     * [get description]
     * @return [type] [description]
     */
    public function get()
    {
        $where = '';

        if ($this->where) {
            $where       = 'WHERE '.implode(' AND ', $this->where);
            $this->where = [];
        }

        $manyids = $this->DB->query("SELECT GROUP_CONCAT(id) AS ids FROM {$this->table} {$where}");
        $results = $this->DB->query("SELECT * FROM {$this->table} {$where}");

        if ($this->hasMany) {
            foreach ($this->hasMany as $relation => $option) {
                $results->{$relation} = (new $option['model'])
                    ->where_in($option['field'], $manyids->fetch()['ids'])
                    ->get();
            }
        }
        return $results ? $results->fetchall(5) : false;
    }

    /**
     * [save description]
     * @param Array $collection [description]
     * @return [type]           [description]
     */
    public function save(Array $collection)
    {
        if (isset($collection['id'])) {
            unset($collection['created']);
            $collection['updated'] = date('Y-m-d H:i:s');
        }
        else {
            $collection['created'] = date('Y-m-d H:i:s');
        }

        $justkeys  = array_keys($collection);
        $field     = implode(', ', $justkeys);
        $keys      = implode(',:', $justkeys);
        $fields    = [];
        $bindParam = [];

        foreach ($collection as $key => $value) {
            $fields   [   $key  ] = "{$key} = :{$key}";
            $bindParam[":{$key}"] = $value;
        }

        unset($fields['id']);

        $fields           = implode(', ', $fields);
        $this->last_query = (!isset($collection['id'])
            ? "INSERT INTO {$this->table} ({$field}) VALUES(:{$keys})"
            : "UPDATE {$this->table} SET {$fields} WHERE id = :id"
        );

        $DB = $this->DB->prepare($this->last_query);

        foreach ($bindParam as $key => $value) {
            $DB->bindParam($key, $value);
        }
        return $DB->execute($collection);
    }

    /**
     * [delete description]
     * @param  integer $id [description]
     * @return [type]      [description]
     */
    public function delete(int $id = 0)
    {
        $DB = $this->DB->prepare("DELETE FROM {$this->table} WHERE id = :id");
        $DB->bindParam('id', $id);

        return $DB->execute($collection);
    }
}
