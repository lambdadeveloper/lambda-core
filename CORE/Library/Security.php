<?php
/**
 * PHP λ::lambda(); // Personal PHP7 Framework
 *
 * PHP version 7
 *
 * @category Framework
 * @package  Lambda
 * @author   John Murowaniecki <jmurowaniecki@gmail.com>
 * @license  Creative Commons 4.0 - Some rights reserved.
 * @link     https://gitlab.com/php-developer/lambda
 **/

namespace CORE\Library;

/**
 * CORE class Security
 *
 * @category CORE_Library
 * @package  Lambda
 * @author   John Murowaniecki <jmurowaniecki@gmail.com>
 * @license  Creative Commons 4.0 - Some rights reserved.
 * @link     https://gitlab.com/php-developer/lambda
 **/
class Security
{

    /**
     * Class constructor.
     *
     * @return Collection
     **/
    public static function getRequestData()
    {
        return $_POST;
    }
}
